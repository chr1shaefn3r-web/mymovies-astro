# My Movies

 > A visualization of my videodb.xml (kodi export) for the web

<div align="center"><img width="55" style="padding-right: 1em" src="https://raw.githubusercontent.com/gilbarbara/logos/master/logos/astro.svg"/><img width="55" src="https://raw.githubusercontent.com/gilbarbara/logos/master/logos/react.svg"/></div>

Icons via [TechStack](https://techstack-logos.web.app/)

## Upgrade dependencies

`npx npm-check-updates --enginesNode --packageManager yarn --doctor -u`

## Commands

All commands are run from the root of the project, from a terminal:

| Command                | Action                                             |
| :--------------------- | :------------------------------------------------- |
| `npm install`          | Installs dependencies                              |
| `npm run dev`          | Starts local dev server at `localhost:3000`        |
| `npm run build`        | Build your production site to `./dist/`            |
| `npm run preview`      | Preview your build locally, before deploying       |
| `npm run astro ...`    | Run CLI commands like `astro add`, `astro preview` |
| `npm run astro --help` | Get help using the Astro CLI                       |