import "./pagetitle.css"

const Pagetitle = ({ title, children }: { title: string, children?: JSX.Element }): JSX.Element => <div className="pageTitle">
    <h1>{title}</h1>
    {children}
</div>;

export default Pagetitle;