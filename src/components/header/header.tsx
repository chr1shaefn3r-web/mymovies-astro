import MainNav from '../nav/mainNav';
import MoviesMainNavigation from '../nav/moviesMainNavigation';
import PageTitle from './pagetitle'

const Header = ({ title }: { title: string }) => <header>
  <MainNav firstLevel={<MoviesMainNavigation title={title} />} />
  <PageTitle title={title} />
</header>;

export default Header;
