import "./mainNav.css"

const MainNav = ({ firstLevel, children }: { firstLevel: JSX.Element, children?: JSX.Element }) => <nav className="mainNav">
    {firstLevel}
    {children}
</nav>;

export default MainNav;