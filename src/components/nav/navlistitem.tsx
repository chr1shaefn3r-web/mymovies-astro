import "./navlistitem.css"

const Navlistitem = ({ title, currentPage, asPath }: { title: string, currentPage: string, asPath?: string }) => <li className={"navlistitem"}>
    <a href={asPath || "/" + title.toLowerCase()} className={title === currentPage ? "active" : ""}>{title}</a>
</li>;

export default Navlistitem;
