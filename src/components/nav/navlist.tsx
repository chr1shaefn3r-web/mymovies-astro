import "./navlist.css"

const Navlist = ({ children }: { children: JSX.Element }) => <ul className={"navlist"}>
    {children}
</ul>;

export default Navlist;