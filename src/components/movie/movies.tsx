import Movie from "./movie";

const Movies = ({ sortedMovies }: {sortedMovies: any[]}) => <div className="movies">
    {
        sortedMovies.map((movie) => <Movie key={movie.id + movie.file} {...movie} />)
    }
</div >;

export default Movies;
