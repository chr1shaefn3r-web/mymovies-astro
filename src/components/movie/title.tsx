import "./title.css"
const Title = ({ title }: {title: string}) => <div className="movieTitleHeader" id={title}>
    <a aria-label="Anchor" href={`#${title}`} >{title}</a>
</div>;

export default Title;
