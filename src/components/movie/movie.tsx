import "./movie.css"
import MoviePoster from "./moviePoster"
import Title from "./title"

const Movie = (
    { id, title, poster, tagline, plot } :
    {id: string, title: string, poster: string, tagline: string, year: string, plot: string}
) => {
    return (
        <div className="movie">
            <MoviePoster id={id} title={title} url={poster} />

            <div className="content">
                <Title title={title} />
                <div className="tagline">{tagline}</div>
                <div className="description">{plot}</div>
            </div>
        </div >
    )
};

export default Movie;
