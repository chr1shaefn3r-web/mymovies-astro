import MoviePoster from "../movie/moviePoster"
import "./collectionEntry.css"

const CollectionEntry = ({ id, title, url }: {id: string, title: string, url: string}) => {

    return (<div className="small-img">
        <MoviePoster id={id} title={title} url={url} />
    </div >)
};

export default CollectionEntry;
