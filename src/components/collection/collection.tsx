import CollectionEntry from "./collectionEntry"
import "./collection.css"
import type { Movie } from "../../api/home/lastAddedMovies";

const Collection = (
    { foundMovies = [] }: { foundMovies: Movie[] }
) => <div className="collection">
        <div className="collectionContent">
            {
                foundMovies.map(({ id, file, title, poster }) =>
                    <CollectionEntry key={id + file} id={id} title={title} url={poster} />
                )
            }
        </div>
    </div >;

export default Collection;
