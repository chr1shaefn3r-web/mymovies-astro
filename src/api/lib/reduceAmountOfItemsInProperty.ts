export default (property: string, amountOfAllowedItems: number) =>
    (item: any) => {
        return Object.entries(item).reduce((acc: any, [key, value]: [any, any]) => {
            if (property === key) {
                acc[key] = value.slice(0, amountOfAllowedItems);
            } else {
                acc[key] = value
            }
            return acc;
        }, {});
    }