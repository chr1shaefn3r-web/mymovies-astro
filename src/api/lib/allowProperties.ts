export default (listOfProperties: string[] = []) =>
    (item: any) => {
        return Object.entries(item).reduce((acc: any, [key, value]) => {
            if (listOfProperties.includes(key)) {
                acc[key] = value;
            }
            return acc;
        }, {});
    }