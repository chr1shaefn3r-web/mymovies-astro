export default (...fns: any) =>
    (x: any) => fns.reduceRight((y: any, f: any) => f(y), x);