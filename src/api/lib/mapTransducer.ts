export default (transform: any) =>
    (nextReducer: any) =>
        (accumulator: any, current: any) => nextReducer(accumulator, transform(current));