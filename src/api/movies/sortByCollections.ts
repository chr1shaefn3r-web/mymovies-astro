import moviesWithoutCollection from './moviesWithoutCollection';
import sortAlphabetically from './sortAlphabetically';

export default (movies = [], collections = []) => {
    if (collections.length === 0) {
        return movies;
    }

    const moviesNotInACollection = moviesWithoutCollection(movies, collections);
    const virtualCollectionForMoviesWithoutOne = moviesNotInACollection.map((movie: any) => {
        return {
            name: movie.title,
            movies: [movie]
        }
    })

    const collectionsForMoviesWithOne = collections.map((collection: any) => {
        collection.movies = collection.movies
            .map(((movieId: string) => movies.find((movie: any) => movie.id === movieId)))
            .filter(Boolean);
        return collection;
    }, []);

    const sortedCollections = sortAlphabetically(collectionsForMoviesWithOne.concat(virtualCollectionForMoviesWithoutOne), "name")
    return sortedCollections.reduce((acc: any, collection: any) => {
        return acc.concat(collection.movies);
    }, []);
}