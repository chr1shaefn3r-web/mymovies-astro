export default (elements: any, propertyName: string) => {
	if (!elements || !Array.isArray(elements)) {
		return [];
	}
	return sort(elements, propertyName);
}

const sort = (elements: any, propertyName: string) => {
	return elements.sort((a: any, b: any) => {
		const first = specialTreatment(a[propertyName]).toLowerCase();
		const second = specialTreatment(b[propertyName]).toLowerCase();
		return (first < second ? -1 : (first > second ? 1 : 0))
	});
};

const specialTreatment = (name: string) => {
	let noPrefix = name.replace(/^(Der|Die|Das|The)/, '');
	let noAUmlaut = noPrefix.replace(/Ä/, 'A');
	let noUUmlaut = noAUmlaut.replace(/Ü/, 'U');
	let noOUmlaut = noUUmlaut.replace(/Ö/, 'O');
	let noBrackets = noOUmlaut.replace(/\(|\)/, '');
	return noBrackets.trim();
};
