import movies from "./data.json";
import moviesByProperties from "./moviesByProperties";
import collectionsFromMovies from "../collections/collectionsFromMovies";
import sortByCollections from "./sortByCollections";

export default () => {
    return sortByCollections(
        moviesByProperties(["id", "file", "title", "poster", "tagline", "year", "plot", "genre", "actors", "runtime", "mediaType", "director"]),
        collectionsFromMovies(movies)
    );
}
