import movies from "./data.json";
import compose from "../lib/compose";
import concat from "../lib/concat";
import allowProperties from "../lib/allowProperties";
import reduceAmountOfItemsInProperty from "../lib/reduceAmountOfItemsInProperty";
import mapTransducer from "../lib/mapTransducer";
import config from "./config";

export default (neededProperties: string[]) => {
    const { significantActors, mainGenres } = config;
    const removeUnneededProperties = mapTransducer(allowProperties(neededProperties));
    const onlySignifacantActors = mapTransducer(reduceAmountOfItemsInProperty("actors", significantActors));
    const onlyMainGenres = mapTransducer(reduceAmountOfItemsInProperty("genre", mainGenres));
    const xform = compose(
        removeUnneededProperties,
        onlySignifacantActors,
        onlyMainGenres
    )(concat);

    return movies.reduce(xform, []);
}
