export default (movies = []) => {
    return sortByKey(movies.reduce((acc: any, movie: any) => {
        if (movie.set && movie.set.name) {
            acc[movie.set.name] = acc[movie.set.name] ? acc[movie.set.name].concat([movie]) : [movie]
            acc[movie.set.name] = acc[movie.set.name].sort((firstMovie: any, secondMovie: any) => firstMovie.year - secondMovie.year)
        }
        return acc;
    }, {}));
};

function sortByKey(collections = []) {
    return Object.entries(collections)
        .map(([name, movies]: [string, any]) => {
            if (movies.length <= 1) {
                return;
            }
            return {
                name,
                movies: movies.map((movie: any) => movie.id)
            };
        })
        .filter(Boolean);
}
