import sortByDateadded from "./sortByDateAdded";
import moviesByProperties from "../movies/moviesByProperties";

export interface Movie {
    id: string;
    title: string;
    poster: string;
    file: string;
}

export default (limit: number): Movie[] => {
    const movies = moviesByProperties(["id", "title", "poster", "dateadded"]);
    return sortByDateadded(movies).slice(0, limit);
}
