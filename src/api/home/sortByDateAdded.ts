export default (movies = []) => {
    return movies.sort((firstMovie, secondMovie) => {
        const firstMovieDate = new Date(getDateAdded(firstMovie)).getTime();
        const secondMovieDate = new Date(getDateAdded(secondMovie)).getTime();
        return secondMovieDate - firstMovieDate;
    });
}

function getDateAdded(movie: any) {
    return movie.dateadded.slice(0, 10);
}