import lastAddedMovies from "./lastAddedMovies";

export default () => {
    return {
        lastAddedMovies: lastAddedMovies(6),
    };
}
